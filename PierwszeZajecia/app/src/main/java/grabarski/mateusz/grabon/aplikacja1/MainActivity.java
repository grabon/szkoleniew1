package grabarski.mateusz.grabon.aplikacja1;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private EditText mEditText1;
    private TextView mTextView1;
    private Button btn1;
    private LinearLayout mLinear;

    private int licznik = 0;

    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            //Pierwsze włączenie aktywności
        } else {
            // Obrót telefonu, po zmianie języka, po restarcie telefonu
        }

        mEditText1 = (EditText) findView(R.id.pole_textowe);
        mTextView1 = (TextView) findViewById(R.id.text_view_1);
        btn1 = (Button) findViewById(R.id.knefel);
        mLinear = (LinearLayout) findViewById(R.id.kontener);

        final LinearLayout.LayoutParams mKontrolkaParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "agf");
//                if (mEditText1.getText().length() == 0 && mEditText1 != null) {
//                    mEditText1.setError("Musisz wpisac tekst");
//                } else {
//                    mTextView1.setText(mEditText1.getText().toString());
//                }
                if (mEditText1.getText() == null || mEditText1.getText().length() < 3) {
                    Toast.makeText(getApplicationContext(), "Za krótki tekst", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    licznik++;
                    mTextView1.setText(mEditText1.getText().toString() + " " + licznik);
//                    mEditText1.setText("");
                    TextView tv = new TextView(getApplicationContext());
                    tv.setLayoutParams(mKontrolkaParam);
                    tv.setTextColor(Color.BLACK);
                    tv.setText(mEditText1.getText().toString() + " " + licznik);
                    mLinear.addView(tv);
                }
            }
        });

//        btn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mEditText1.getText().length() == 0) {
//                    mEditText1.setError("Musisz wpisac tekst");
//                } else {
//                    mTextView1.setText(mEditText1.getText().toString());
//                }
//            }
//        });


//        widokZKodu();
    }

    public void widokZKodu() {
        mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        mLinear.setLayoutParams(mParams);

        //wspolne atrybuty dla edittext i textview
        final LinearLayout.LayoutParams mKontrolkaParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mEditText1 = new EditText(this);
        mTextView1 = new TextView(this);

        mEditText1.setLayoutParams(mKontrolkaParam);
        mEditText1.setSingleLine(true);
        mEditText1.setHint(R.string.moje_zasoby_text1);

        mTextView1.setLayoutParams(mKontrolkaParam);
        mTextView1.setTextColor(Color.GREEN);

        btn1 = new Button(this);

        LinearLayout.LayoutParams mGuzikParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btn1.setLayoutParams(mGuzikParam);
        btn1.setText("GOTOWE!");
        btn1.setTextSize(18);

        String a = getResources().getString(R.string.moje_zasoby_text1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "agf");
//                if (mEditText1.getText().length() == 0 && mEditText1 != null) {
//                    mEditText1.setError("Musisz wpisac tekst");
//                } else {
//                    mTextView1.setText(mEditText1.getText().toString());
//                }
                if (mEditText1.getText() == null || mEditText1.getText().length() < 3) {
                    Toast.makeText(getApplicationContext(), "Za krótki tekst", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    licznik++;
                    mTextView1.setText(mEditText1.getText().toString() + " " + licznik);
//                    mEditText1.setText("");
                    TextView tv = new TextView(getApplicationContext());
                    tv.setLayoutParams(mKontrolkaParam);
                    tv.setText(mEditText1.getText().toString() + " " + licznik);
                    mLinear.addView(tv);
                }
            }
        });

        mLinear.addView(mEditText1);
        mLinear.addView(mTextView1);
        mLinear.addView(btn1);
        this.setContentView(mLinear);

//        ((LinearLayout) findViewById(R.id.kontener)).addView(mLinear);
    }

    //dzieki tej metodzie nie trzeba rzutować
    public <T extends View> T findView(int id) {
        return (T) findViewById(id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystko) {
            czyszczenieWidoku();
            return true;
        } else if (id == R.id.usun_ostatni) {
            usunOstatani();
            return true;
        } else if (id == R.id.otwieranie_listy) {
            Intent intent = new Intent(this, Listy.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void usunOstatani() {
        final View mChild = mLinear.getChildAt(mLinear.getChildCount() - 1);
        if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)) {

            ObjectAnimator moveAnim = ObjectAnimator.ofFloat(mChild, "Y", 1000);
            moveAnim.setDuration(2000);
            moveAnim.setInterpolator(new BounceInterpolator());
            moveAnim.start();

            moveAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mLinear.removeView(mChild);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    protected void czyszczenieWidoku() {

        List<View> mkolekacja = new ArrayList<View>();

        for (int i = 0; i < mLinear.getChildCount(); i++) {
            View mChild = mLinear.getChildAt(i);
            mkolekacja.add(mChild);
        }

        for (View mChild : mkolekacja) {
            if (mChild.getClass().equals(TextView.class)) {
                if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)) {
                    mLinear.removeView(mChild);
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}