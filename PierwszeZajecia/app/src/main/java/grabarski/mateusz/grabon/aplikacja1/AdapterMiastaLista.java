package grabarski.mateusz.grabon.aplikacja1;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by grabon on 12.04.15.
 */
public class AdapterMiastaLista extends ArrayAdapter<String> {

    private LayoutInflater minflater;
    private List<String> selectedList;

    public AdapterMiastaLista(Context context, List<String> dane) {
        super(context, R.layout.list_element_miasto, dane);
        this.minflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.selectedList = new ArrayList<String>(dane.size());
    }

    public List<String> returnZaznaczone(){
        return Collections.unmodifiableList(selectedList); //zwaraca kopie danych, których nie można modyfikować
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MiastoViewHolder mHolder = new MiastoViewHolder();

        if (convertView == null) {
            convertView = minflater.inflate(R.layout.list_element_miasto, parent, false);
            TextView mNazwaMiasta = (TextView) convertView.findViewById(R.id.nazwa_miasta);
            CheckBox zaznaczony = (CheckBox) convertView.findViewById(R.id.checkbox);

            mHolder.miasto = mNazwaMiasta;
            mHolder.zaznaczony = zaznaczony;

            convertView.setTag(mHolder);
        } else {
            mHolder = (MiastoViewHolder) convertView.getTag(); //pobieramy viewHolder powiązany już z tym widokiem
        }

//        View wiersz = minflater.inflate(R.layout.list_element_miasto, parent, false); //tworzenie widoku za pomocą inflatera. Inflater tworzy z pliku xml plik javowy, false - oznacze, że ma nie dodawać parenta

        //adapter też liczy od zera, dlatego można pobierać pozycję od 0
        final String mMiasto = getItem(position);
        ((MiastoViewHolder) convertView.getTag()).miasto.setText(mMiasto);//
        mHolder.miasto.setText(mMiasto);
        mHolder.zaznaczony.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedList.add(mMiasto); //dodanie do zaznaczonych
                } else {
                    selectedList.remove(mMiasto); //usunięcie z zaznaczonych
                }
                Log.d("MiastoListAdapter", "Zaznaczono/odchaczono");
            }
        });

//        TextView textView = (TextView) wiersz.findViewById(R.id.nazwa_miasta);
//        textView.setText(mMiasto);
        return convertView;
    }

    //handler widoku
    public static class MiastoViewHolder {
        public TextView miasto;
        public CheckBox zaznaczony;

    }
}
