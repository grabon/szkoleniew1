package grabarski.mateusz.grabon.aplikacja1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grabon on 12.04.15.
 */
public class Listy extends ActionBarActivity {

    private List<String> aList = new ArrayList<>();
    private ListView aListView;
    private AdapterMiastaLista adapterMiastaLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listy);

        aListView = (ListView) findViewById(R.id.lista_miast);

        zaladujListe();

        //aListView.addFooterView(); ustawia dodatkowy widok jako ostatni element i to jest stopka
        aListView.addHeaderView(getHeaderButton());
        adapterMiastaLista = new AdapterMiastaLista(this, aList);
        aListView.setAdapter(adapterMiastaLista);
    }

    private void zaladujListe() {
        aList.add("Warszawa");
        aList.add("Wrocław");
        aList.add("Poznań");
        aList.add("Knurów");
        aList.add("Gliwice");
    }

    protected Button getHeaderButton() {

        Button button = new Button(this);
        button.setText("Wyślij SMS!");
        button.setLayoutParams(
                new AbsListView.LayoutParams(
                        AbsListView.LayoutParams.MATCH_PARENT,
                        AbsListView.LayoutParams.WRAP_CONTENT));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> mzaznaczone = adapterMiastaLista.returnZaznaczone();
                StringBuilder stringBuilder = new StringBuilder();

                for (String mZaznaczone : mzaznaczone){
                    stringBuilder.append(mzaznaczone);
                    stringBuilder.append(", ");
                }

                String finalResault = stringBuilder.substring(0, stringBuilder.lastIndexOf(", "));

                Toast.makeText(Listy.this, stringBuilder, Toast.LENGTH_SHORT).show();

                //daje możliwość wysłania smsa z gotową treścią
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.putExtra("sms_body", finalResault);
//                intent.setType("vnd.android-dir/mms-sms");
//                startActivity(intent);

                //inna metodya wysyłania smsa
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
                intent.putExtra("sms_body", finalResault);
                startActivity(intent);
            }
        });
        return button;
    }
}
