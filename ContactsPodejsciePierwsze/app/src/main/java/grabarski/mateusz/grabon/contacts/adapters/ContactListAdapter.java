package grabarski.mateusz.grabon.contacts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import grabarski.mateusz.grabon.contacts.R;
import grabarski.mateusz.grabon.contacts.models.Contact;

/**
 * Created by grabon on 18.04.15.
 */
public class ContactListAdapter extends ArrayAdapter<String> {

    private ArrayList<String> contacts;
    private LayoutInflater inflater;

    public ContactListAdapter(Context context, ArrayList<String> contacts) {
        super(context, R.layout.contact_row, contacts);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.contacts = new ArrayList<String>(contacts.size());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contact_row, parent, false);
            TextView nazwaKontaktu = (TextView) convertView.findViewById(R.id.name_string);

            holder.textView = nazwaKontaktu;

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String kontakt = getItem(position);
        holder.textView.setText(kontakt);

        return convertView;
    }

    public static class ViewHolder{
        public TextView textView;
    }
}
