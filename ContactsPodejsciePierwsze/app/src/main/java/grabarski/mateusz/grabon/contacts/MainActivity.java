package grabarski.mateusz.grabon.contacts;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import grabarski.mateusz.grabon.contacts.adapters.ContactListAdapter;
import grabarski.mateusz.grabon.contacts.models.Contact;


public class MainActivity extends ActionBarActivity {

    private Contact contact;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.lista_kontaktow);

        contact = new Contact();
        generowanieDanych();

        ContactListAdapter contactListAdapter = new ContactListAdapter(this, contact.generateContactList());

        listView.setAdapter(contactListAdapter);

    }

    private void generowanieDanych() {
        contact.addContact("Kontakt1");
        contact.addContact("Kontakt2");
        contact.addContact("Kontakt3");
        contact.addContact("Kontakt4");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
