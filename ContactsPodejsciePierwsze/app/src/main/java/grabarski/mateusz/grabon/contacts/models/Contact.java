package grabarski.mateusz.grabon.contacts.models;

import java.util.ArrayList;

/**
 * Created by grabon on 18.04.15.
 */
public class Contact {
    private String name;
    private ArrayList<String> contactList;

    public Contact() {
        contactList = new ArrayList<String>();
    }

    public void addContact(String name) {
        contactList.add(name);
    }

    public ArrayList<String> generateContactList() {
        return this.contactList;
    }
}
