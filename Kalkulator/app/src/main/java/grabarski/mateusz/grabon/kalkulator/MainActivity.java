package grabarski.mateusz.grabon.kalkulator;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private EditText ed1, ed2;
    private TextView resault;
    private Button dodawanie, odejmowanie, mnozenie, dzielenie;

    private int a, b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ed1 = (EditText) findViewById(R.id.arg_1);
        ed2 = (EditText) findViewById(R.id.arg_2);
        resault = (TextView) findViewById(R.id.wynik);

        dodawanie = (Button) findViewById(R.id.dodawanie);
        odejmowanie = (Button) findViewById(R.id.odejmowanie);
        mnozenie = (Button) findViewById(R.id.mnozenie);
        dzielenie = (Button) findViewById(R.id.dzielenie);

        dodawanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(ed1.getText()) || TextUtils.isEmpty(ed2.getText())) {
                    Toast.makeText(MainActivity.this, "Brak danych!", Toast.LENGTH_SHORT).show();
                    return;
                }

                a = Integer.parseInt(ed1.getText().toString());
                b = Integer.parseInt(ed2.getText().toString());
                int res = a + b;

                resault.setText("" + res);
            }
        });

        odejmowanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(ed1.getText()) || TextUtils.isEmpty(ed2.getText())) {
                    Toast.makeText(MainActivity.this, "Brak danych!", Toast.LENGTH_SHORT).show();
                    return;
                }

                a = Integer.parseInt(ed1.getText().toString());
                b = Integer.parseInt(ed2.getText().toString());
                int res = a - b;

                resault.setText("" + res);
            }
        });

        mnozenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(ed1.getText()) || TextUtils.isEmpty(ed2.getText())) {
                    Toast.makeText(MainActivity.this, "Brak danych!", Toast.LENGTH_SHORT).show();
                    return;
                }

                a = Integer.parseInt(ed1.getText().toString());
                b = Integer.parseInt(ed2.getText().toString());
                int res = a * b;

                resault.setText("" + res);
            }
        });

        dzielenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(ed1.getText()) || TextUtils.isEmpty(ed2.getText())) {
                    Toast.makeText(MainActivity.this, "Brak danych!", Toast.LENGTH_SHORT).show();
                    return;
                }

                a = Integer.parseInt(ed1.getText().toString());
                b = Integer.parseInt(ed2.getText().toString());
                int res = a / b;

                resault.setText("" + res);
            }
        });


    }
}
