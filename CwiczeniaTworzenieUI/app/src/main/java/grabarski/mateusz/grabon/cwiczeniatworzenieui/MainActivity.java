package grabarski.mateusz.grabon.cwiczeniatworzenieui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private List<String> zadania = new ArrayList<>();

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glowna);

        listView = (ListView) findViewById(R.id.zadania);
        generowanieDanych();

        listView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, zadania));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if (position == 0){
                    intent = new Intent(MainActivity.this, Cwiczenie1.class);
                    startActivity(intent);
                } else if (position == 1){
                    intent = new Intent(MainActivity.this, Cwiczenie2.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void generowanieDanych() {
        zadania.add("3 guziki i linearLayout");
        zadania.add("Zagnieżdzanie widoków");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
