package grabarski.mateusz.grabon.przekazywaniedanychmiedzyaktywnosciami;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by grabon on 15.04.15.
 */
public class SecoundActivity extends Activity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);

        textView = (TextView) findViewById(R.id.textView);
        textView.setText(getIntent().getExtras().getString("text"));

        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecoundActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
