package grabarski.mateusz.grabon.myapplication.model;

/**
 * Created by grabon on 18.04.15.
 */
public class Contact {

    private String name;
    private boolean zaznaczony;

    public Contact(String name, boolean zaznaczony) {
        this.name = name;
        this.zaznaczony = zaznaczony;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isZaznaczony() {
        return zaznaczony;
    }

    public void setZaznaczony(boolean zaznaczony) {
        this.zaznaczony = zaznaczony;
    }
}
