package grabarski.mateusz.grabon.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import grabarski.mateusz.grabon.myapplication.adapters.ContactListaAdapter;
import grabarski.mateusz.grabon.myapplication.model.Contact;


public class MainActivity extends ActionBarActivity {

    private ArrayList contact;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.lista);

        contact = new ArrayList<Contact>();
        generateData();

        ContactListaAdapter contactListaAdapter = new ContactListaAdapter(this, contact);

        listView.setAdapter(contactListaAdapter);
    }

    private void generateData() {
        contact.add(new Contact("Some name 0", false));
        contact.add(new Contact("Some name 1", false));
        contact.add(new Contact("Some name 2", false));
        contact.add(new Contact("Some name 3", false));
        contact.add(new Contact("Some name 4", false));
        contact.add(new Contact("Some name 5", false));
        contact.add(new Contact("Some name 6", false));
        contact.add(new Contact("Some name 7", false));
        contact.add(new Contact("Some name 8", false));
        contact.add(new Contact("Some name 9", false));
        contact.add(new Contact("Some name 10", false));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
