package grabarski.mateusz.grabon.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import grabarski.mateusz.grabon.myapplication.R;
import grabarski.mateusz.grabon.myapplication.model.Contact;

/**
 * Created by grabon on 18.04.15.
 */
public class ContactListaAdapter extends ArrayAdapter<Contact> {

    private ArrayList<Contact> lista;
    private LayoutInflater inflater;

    public ContactListaAdapter(Context context, ArrayList<Contact> dane) {
        super(context, R.layout.contact_row, dane);
        this.lista = dane;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.contact_row, parent, false);
            TextView tv = (TextView) convertView.findViewById(R.id.name);
            CheckBox cb = (CheckBox) convertView.findViewById(R.id.checked);

            viewHolder.name = tv;
            viewHolder.selected = cb;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(lista.get(position).getName());
        viewHolder.selected.setChecked(lista.get(position).isZaznaczony());
        viewHolder.selected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    lista.get(position).setZaznaczony(true);
                } else {
                    lista.get(position).setZaznaczony(false);
                }
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        public TextView name;
        public CheckBox selected;
    }
}
